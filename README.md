What is this?
-------------
This is a simple setup of drf-spectacular and swagger-ui using Docker.


RUN
---
To run this make sure you have Docker setup, then just run `docker compose -f local.yml up`
The swagger-ui should be accessible from *http://localhost:80*.

