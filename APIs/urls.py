from django.urls import path
from .views import ListUserview

urlpatterns = [
    path("listuser", ListUserview.as_view(), name="list_user"),
]
