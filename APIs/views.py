from rest_framework.generics import ListAPIView
from django.contrib.auth.models import User
from .serializers import UserSerializer


class ListUserview(ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
